#!/usr/bin/python3
from sys import argv
#Funcion suma
def suma(n, m):
    suma = str(n+ m)
    print("La suma de {} y {} es {}".format(n, m, suma))
#Funcion resta
def resta(n, m):
    resta = str(n-m)
    print("La resta de {} y {} es {}".format(n, m, resta))
if __name__ =="__main__":
    try:
        if (argv[1]).lower() == "suma":
            suma(int(argv[2]), int(argv[3]))
            suma(int(argv[4]), int(argv[5]))
        elif (argv[1]).lower() == "resta":
            resta(int(argv[2]), int(argv[3]))
            resta(int(argv[4]), int(argv[5]))

        else:
            print("Debe de introducir suma o resta seguido de cuatro numeros. Ejemplo: suma 2 3 5 6")
    except ValueError:
        print("Estas introducciendo mal algún valor.Ejemplo: resta 5 3 4 2")
    except IndexError:
        print("Debes de escribir sumar o resta seguido cuatro numeros. Ejemplo: suma 1 2 3 4")
